# bigping

***

## Name
bigping

## Description
Ping hosts/network by ip or network/prefix with or without DNS resolution  
Filter only live hosts or dead hosts not answer to ping  
Use system ping-tool

Runs on Linux(RedHat) and Windows system

## Installation
$ pip install bigping  
$ yum localinstall bigping-main-0.1.0-release.noarch.rpm  

## Usage  
Run as module $ **python -m bigping**  
Run as script $ **bigping.py**

~~~

usage: bigping.py [-h] [-v] [-n] [-i INTERFACE] [-l] [-s SELECTION] [-f FILE]

Ping hosts.

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -n, --dns             use name resolving
  -i INTERFACE, --interface INTERFACE
                        use interface
  -l, --list            list all interface
  -s SELECTION, --select SELECTION
                        print only hosts that is UP or DOWN
  -f FILE, --file FILE  input file with ipv4 addresses

This script can take a file with ipv4 addresses or networks. One address/network per line.
The form should be either a specific address, 192.168.1.1 or block 192.168.0.0/24.

Default behavior is to ping all hosts connected to the same network as the first network interface.
Host responding to ping can be resolved in DNS.

Script runs with max 254 threads, i.e C-block will be pinged
in parallel in 254 threads.

Example on this host (myhost.com):
    iface = ens192
    address = 172.30.207.119
    mask = 255.255.252.0
    network = 172.30.204.0/22

    number of hosts in 172.30.204.0/22 is 1022

Example:
    # Default behavior
    > bigping

    # list all availble interface
    > bigping -l

    # print only host that are UP
    > bigping -s UP

    # ping all address via interface wlp2s0
    > bigping -i wlp2s0

    # File with addresses
    > bigping --file ip-addresses.txt

    # pipe addresses to script
    > cat ip-addresses.txt | bigping

    # echo addresses to script
    > echo 10.0.0.0/24 | bigping

~~~

## Authors and acknowledgment
Svante Viktorsson  

## License
See LICENSE  

## Project status
Released  
