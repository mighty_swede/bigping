.PHONY: init-venv venv clean clean-all validate test pylint bandit rpm wheel debug dist all

.DEFAULT_GOAL   := all
SOURCES         := src/bigping/bigping.py
PROD_SOURCE     := src/bigping/bigping.py
PYTEST_STD_OPTS := -v --log-level=warning --ignore deps

init-venv:
	@command -v python3 > /dev/null 2>&1 || { echo "python3 required but not found."; exit 1; }
	@[ -d venv ] || { \
		echo "Initializing venv for python3"; python3 -m venv venv; \
		source venv/bin/activate; \
		echo "Upgrading pip"; \
		pip install --upgrade pip; \
	}
	@[ -d build ] || mkdir build

venv: init-venv
	@(source venv/bin/activate; \
		echo "Installing/upgrading depdendencies"; \
		pip install -qr requirements.txt)

clean:
	@(find . -maxdepth 1 -name '*.egg-info' -type d -exec rm -rf {} \;)
	@[ -d dist ] && { echo "Removing dist"; rm -rf dist; } || true
	@[ -d build ] && { echo "Removing build"; rm -rf build; } || true
	@echo "Removing __pycache__ dirs"; find . -depth -type d -name __pycache__ -exec rm -rf {} \; || true
	@[ -d src/bigping.egg-info ] && { echo "Removing src/bigping.egg-info"; rm -rf src/bigping.egg-info; } || true

clean-all: clean
	@[ -d venv ] && { echo "Removing venv"; rm -rf venv; } || true
	@[ -d .pytest_cache ] && { echo "Removing .pytest_cache"; rm -rf .pytest_cache; } || true
	@[ -d .bigping.egg-info ] && { echo "Removing .bigping.egg-info"; rm -rf .bigping.egg-info; } || true


validate: pylint bandit

test:

pylint: venv
	@(source venv/bin/activate; echo "pylint $(SOURCES)"; pylint $(SOURCES))

bandit: venv
	@(source venv/bin/activate; echo "bandit -r $(PROD_SOURCE)"; bandit -r $(PROD_SOURCE))

rpm: RELEASE := $$(git branch --show-current)
rpm: clean sdist
	mkdir -p build/rpm/{SPECS,SOURCES}; \
	cp bin/bigping build/rpm/SOURCES; \
	cp dist/*.tar.gz build/rpm/SOURCES; \
	cp spec/bigping.spec build/rpm/SPECS; \
	echo "rpmbuild build/rpm/SPECS/bigping.spec"; \
	rpmbuild -bb \
	    --define "_topdir $$PWD/build/rpm" \
		--define "rpmname bigping" \
		--define "provided_name bigping" \
		--define "version $$(cat VERSION)" \
		--define "release $(RELEASE)" \
		build/rpm/SPECS/bigping.spec; \
	cp $$PWD/build/rpm/RPMS/noarch/*.rpm dist

wheel: clean
	@(echo "python3 -m build --wheel"; python3 -m build --wheel;)

sdist:
	@(echo "python3 -m build --sdist"; python3 -m build --sdist;)

release: rpm wheel

debug: RELEASE := debug
debug: rpm wheel

dist: validate test release

all: validate test rpm wheel