#!/usr/bin/env python3
######################################################
#
# author: svante.victorsson@gmail.com
# license: GPLv3
#
# ping a list of host with threads for increase speed
# use standard linux /bin/ping utility.
# runs under linux/windows
#
######################################################

import sys
import queue
import re
import subprocess # nosec
import socket
import argparse
import platform
import signal
from ipaddress import IPv4Network
from threading import Thread, Lock, Event, main_thread
from threading import enumerate as th_enumerate
from sys import stdin
from os import isatty, environ
import psutil


MIN_PYTHON = (3, 6)
if sys.version_info < MIN_PYTHON:
    sys.exit(f'Python version: {MIN_PYTHON} or later is required.\n'
             f'Current Version isr: ({sys.version_info.major}, {sys.version_info.minor})')

af_map = {
    socket.AF_INET: 'IPv4',
    socket.AF_INET6: 'IPv6',
    psutil.AF_LINK: 'MAC',
}

__VERSION_INFO__ = "latest"

# global variables
MAX_NUMBER_OF_THREADS = 256
COUNTER = 0
LENGTH = 0
INCREMENT = 1

# lock thread on increment
mutex = Lock()

# set color red text
def RED(text):  # pylint: disable=C0103
    """ Set color for string """
    return '\033[0;31m' + text + '\033[0m'


# set color green text
def GREEN(text):  # pylint: disable=C0103
    """ Set color for string """
    return '\033[0;92m' + text + '\033[0m'


# set color white text
def WHITE(text):  # pylint: disable=C0103
    """ Set color for string """
    return '\033[1;37m' + text + '\033[0m'


# return name or empty string
def getname(name):
    """ Handy function to set empty string """
    if name == 'None':
        return ''
    return name


# pylint: disable=C0103
# thread code : wraps system ping command
def thread(e, i, q_in, q_out, resolve):  # pylint: disable=W0613,R0912,R0914
    """Pings hosts in queue"""
    global COUNTER  # pylint: disable=W0603

    # ping program
    if platform.system() == "Windows":
        ping_prog = "C:/Windows/system32/ping.exe"
    elif platform.system() == "Linux":
        ping_prog = "/usr/bin/ping"
    else:
        q_out.put("OS Not supported - exiting sub")
        return

    while True:
        # get an IP item form queue
        ip = q_in.get()
        ping_args = [ping_prog, '-c', '2', '-W', '1', str(ip)]
        if resolve:
            try:
                hostname = socket.gethostbyaddr(str(ip))[0]
            except (OSError, socket.gaierror) as err:  # pylint: disable=W0612
                hostname = "NXDOMAIN"
        else:
            hostname = None
        with subprocess.Popen(ping_args, shell=False, stdout=subprocess.PIPE,  # nosec
                              stderr=subprocess.DEVNULL) as proc:  # nosec
            # save ping stdout
            ping_result = proc.wait()
            p_ping_out = str(proc.stdout.readlines())
            event_is_set = e.is_set()
            if event_is_set: # if event return immediately
                q_in.task_done()
                return

            if ping_result == 0:
                if platform.system() == "Windows":
                    # Approximate round trip times in milli-seconds:
                    #  Minimum = 5ms, Maximum = 8ms, Average = 6ms
                    search = re.search(r'Minimum = (.*), Maximum = (.*), Average = ([\w]+)',
                                       p_ping_out, re.M | re.I)
                    ping_rtt = search.group(3)
                elif platform.system() == "Linux":
                    # rtt min/avg/max/mdev = 22.293/22.293/22.293/0.000 ms
                    search = re.search(r'rtt min/avg/max/mdev = (.*)/(.*)/(.*)/(.*) ms',
                                       p_ping_out, re.M | re.I)
                    ping_rtt = search.group(2)
                    q_out.put("UP " + str(ip) + " " + ping_rtt + " " + str(hostname))
            elif ping_result == 1:
                q_out.put("DOWN " + str(ip))
            else:
                q_out.put("ERROR " + str(ip))
                # update queue : this ip is processed
            with mutex:
                COUNTER += INCREMENT
                print_progressbar(COUNTER, LENGTH, prefix='Progress:', suffix='Complete', length=50)
                q_in.task_done()
        if q_in.empty():
            break


def validate_address(address, ips):
    """ Try to validate address """
    try:
        net = IPv4Network(address)
        if net.prefixlen == 32:
            ips.append(net[0])
        else:
            for host in net.hosts():
                ips.append(host)
    except ValueError:
        print(f'address/netmask is invalid for IPv4:{address}')
        sys.exit(-1)


def get_all_hosts_in_network(ips, network):
    """ Get all hosts on network """
    for host in network.hosts():
        ips.append(host)


def get_all_interfaces():
    """ Try to fetch all local interfaces """
    lst = []
    for nic, addrs in psutil.net_if_addrs().items():
        for addr in addrs:
            if af_map.get(addr.family, addr.family) == "IPv4":
                if IPv4Network(addr.address).overlaps(IPv4Network("169.254.0.0/16")):
                    # print("Interface assigned address according to RFC rfc3927")
                    break
                if IPv4Network(addr.address).is_loopback:
                    # print("Loopback interface ", addr.address)
                    break
                lst.append((nic, addr.address, addr.netmask))
    return lst


def printout(result):
    """ Print out result """
    items = sorted(result.items(), key=lambda item: socket.inet_aton(item[0]))
    #
    # do the printouts
    #
    dash = '-' * 45

    for index, value in enumerate(items):
        # status ip
        data = result[items[index][0]]
        ip = value[0]
        if index == 0:
            print(dash)
            print(f'{"Status":<8s}{"Address":<17s}{"rtt(ms)":<8s}{"Dns":<12s}')
            print(dash)
        if data['rtt']:
            print(f'{GREEN(data["status"]):<19s}{ip:<17s}{data["rtt"]:<8s}'
                  f'{getname(data["name"]):<6s}')
        else:
            print(f'{RED(data["status"]):<19s}{ip:<17s}')


def main():  # pylint: disable=R0912,R0913,R0914,R0915
    """ main """
    # global variables
    global LENGTH  # pylint: disable=W0603

    # local variables
    ips = []
    result = {}
    ips_q = queue.Queue()
    out_q = queue.Queue()
    number_of_threads = MAX_NUMBER_OF_THREADS

    ifaces = get_all_interfaces()
    address = ifaces[0][1]  # ifs[0][1] is 127.0.0.1
    mask = ifaces[0][2]  # ifs[0][2] is 255.0.0.0
    network = IPv4Network(address + "/" + mask, False)
    # handy event
    my_event = Event()

    # define signal handler
    def signal_handler(sig, frame):
        """ Catch ctrl-c """
        signal.signal(signal.SIGINT, signal.SIG_IGN)
        print(f'\nProgram interrupted. Got signal: {signal.Signals(sig).name} \nFrame: {frame}'
              f' - Exiting\n')
        print("Terminate all threads. WAIT!")
        my_event.set()
        main_th = main_thread()
        for my_thread in th_enumerate():
            if not main_th:
                my_thread.join()
        print("Done - exit")
        sys.exit(0)

    # set signal - only SIGINT ctrl-c
    signal.signal(signal.SIGINT, signal_handler)

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description="Ping hosts.",
                                     epilog="""
This script can take a file with ipv4 addresses or networks. One address/network per line.
The form should be either a specific address, 192.168.1.1 or block 192.168.0.0/24.

Default behavior is to ping all hosts connected to the same network as the first network interface.
Host responding to ping can be resolved in DNS.

Script runs with max """ + str(number_of_threads) + """ threads, i.e C-block will be pinged
in parallel in 254 threads.

Example on this host (""" + socket.gethostname() + """):
    iface = """ + WHITE(ifaces[0][0]) + """
    address = """ + WHITE(ifaces[0][1]) + """
    mask = """ + WHITE(ifaces[0][2]) + """
    network = """ + WHITE(str(network)) + """

    number of hosts in """ + str(network) + """ is """ + str(network.num_addresses - 2) + """

Example:
    # Default behavior
    > bigping

    # list all availble interface
    > bigping -l

    # print only host that are UP
    > bigping -s UP

    # ping all address via interface wlp2s0
    > bigping -i wlp2s0

    # File with addresses
    > bigping --file ip-addresses.txt

    # pipe addresses to script
    > cat ip-addresses.txt | bigping

    # echo addresses to script
    > echo 10.0.0.0/24 | bigping
TODO:
    Version  """ + str(__VERSION_INFO__) + """
    @svante.victorsson@gmail.com
""")

    parser.add_argument('-v', '--version', action='version', version='bigping ' +
                                                                     str(__VERSION_INFO__))
    parser.add_argument('-n', '--dns', action='store_true', dest='DNS',
                        help='use name resolving')
    parser.add_argument('-i', '--interface', dest='interface', action='store',
                        help='use interface')
    parser.add_argument('-l', '--list', action='store_true',
                        help='list all interface')
    parser.add_argument('-s', '--select', dest='selection', action='store',
                        help='print only hosts that is UP or DOWN')
    parser.add_argument('-f', '--file', dest='file', action='store',
                        help='input file with ipv4 addresses')
    args = vars(parser.parse_args())

    if args['list']:
        for iface, net, mask in ifaces:
            print("Interface:", iface, IPv4Network(net + "/" + mask, False))
        sys.exit()
    if args['selection']:
        if args['selection'].lower() != 'UP'.lower() and args['selection'].lower()\
                != 'DOWN'.lower():

            parser.print_usage()
            sys.exit()
    if args['interface']:
        address = ""
        mask = ""
        network = ""
        for iface, addr, netmask in ifaces:
            if iface == args['interface']:
                address = addr
                mask = netmask
                network = IPv4Network(address + "/" + mask, False)
                break
        if not address and not mask and not network:
            print("Interface ", args['interface'], " not found. Try one of these:")
            for iface, net, mask in ifaces:
                print("Interface:", iface, IPv4Network(net + "/" + mask, False))
            sys.exit()

    # check if run under pycharm or from real tty
    if 'PYCHARM_HOSTED' not in environ:
        if not isatty(stdin.fileno()):
            # piped
            for line in stdin:
                part = line.split()
                for parts in part:
                    validate_address(parts, ips)
        else:
            # not piped
            if args['file']:
                try:
                    with open(args['file'], "r",encoding="utf-8") as fp:
                        for line in fp:  # pylint: disable=R0914
                            validate_address(line.strip(), ips)
                    fp.close()
                except IOError:
                    # do what you want if there is an error with the file opening
                    print(f'File {RED(args["file"])} IOError. Exiting...')
                    sys.exit()
            else:
                get_all_hosts_in_network(ips, network)
    else:
        # within pycharm IDE
        get_all_hosts_in_network(ips, network)
    #
    # adjust number of threads if less than c-net i.e 254 hosts
    #
    if len(ips) < MAX_NUMBER_OF_THREADS:
        number_of_threads = len(ips)
    else:
        number_of_threads = MAX_NUMBER_OF_THREADS
    LENGTH = len(ips)
    # Initial call to print 0% progress
    print_progressbar(0, LENGTH, prefix='Progress:', suffix='Complete', length=50)

    #
    # start the thread pool
    #
    for i in range(number_of_threads):
        with mutex:
            worker = Thread(target=thread, args=(my_event, i, ips_q, out_q, args['DNS']))
            worker.setDaemon = True
            worker.start()

    # fill queue
    for ip in ips:
        ips_q.put(ip)

    # wait until worker threads are done to exit
    ips_q.join()

    while True:
        try:
            res = out_q.get_nowait()
            status, ip, *rest = res.split()
            if not rest:  # i.e down
                if args['selection'] is None or args['selection'].lower() == 'DOWN'.lower():
                    result[ip] = {'status': status, 'rtt': '', 'name': ''}
            else:
                if args['selection'] is None or args['selection'].lower() == 'UP'.lower():
                    result[ip] = {'status': status, 'rtt': rest[0], 'name': rest[1]}
        except queue.Empty:
            break
    printout(result)


# Print iterations progress
# pylint: disable=R0913
def print_progressbar(iteration, total, prefix='', suffix='', decimals=0, length=100,
                     fill='#', printend="\r"):
    """
    https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printend    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filled_length = int(length * iteration // total)
    bar = fill * filled_length + '-' * (length - filled_length)  # pylint: disable=C0104
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=printend)
    # Print New Line on Complete
    if iteration == total:
        print()


if __name__ == "__main__":
    # execute only if run as a script
    main()
