%define distname bigping

Summary: bigping
Packager: Svante Viktorsson
Name: %{rpmname}
Version: %{version}
Release: %{release}
Source0: %{distname}-%{version}.tar.gz
Source1: bigping
Group: Application/network
BuildRoot: %{_tmppath}/%{distname}-%{version}-%{release}-buildroot
License: Copyright (c) Svante Viktorsson
Prefix: %{_prefix}
BuildArch: noarch
Requires: python >= 3.6 python3-psutil
BuildRequires: python3-devel python3-rpm-macros python3-toml python3-wheel

%global _description %{expand:
A python module which provides a advanced pingtool - ping sweep small/large network
}
%description -n %{rpmname} %_description

%prep
%setup -n %{distname}-%{version}

%generate_buildrequires
%pyproject_buildrequires

%build
%pyproject_wheel

%install
pwd
%pyproject_install
install -D %SOURCE1  %{buildroot}/usr/local/bin/bigping
%pyproject_save_files %{rpmname}

%files -n %{rpmname} -f %{pyproject_files}
%defattr(-,root,root,0555)
%attr(0755, root, root) /usr/local/bin/bigping
